DELIMITER $$

USE `hsu_analysis`$$

DROP PROCEDURE IF EXISTS `create_instance`$$

CREATE DEFINER=`root`@`%` PROCEDURE `create_instance`(
	IN fromtype VARCHAR(20),
	IN fromvalue INT(2),
	IN dftype VARCHAR(20),
	IN dfval INT(2)
    )
BEGIN
	SET @stmt1:= (CONCAT('select DATE_ADD(DATE_sub(curdate(), INTERVAL ',fromvalue,'-1 ',fromtype,' ), interval -day(DATE_SUB(NOW(), INTERVAL ',fromvalue,'-1 ',fromtype,' )) +1 day) INTO @startDate'));
	PREPARE stmt FROM @stmt1;
	EXECUTE stmt;
	SELECT @startDate;
	DEALLOCATE PREPARE stmt;
	
	INSERT INTO rfm_instance(`RFM_INSTANCE_NAME`,`FROM_VALUE`, `TO_VALUE`, `STATUS`, `FROM_TYPE`, `FROMVAL`, `TO_TYPE`, `TOVAL`)
	SELECT CONCAT('Past ',fromvalue,' ', fromtype,' data from ',dfval,' ', dftype) AS NAM, 
	@startDate, NOW() AS TOVAL, 'Created' AS st, fromtype, fromvalue, dftype, dfval ;
	
    END$$

DELIMITER ;