DELIMITER $$

USE `hsu_analysis`$$

DROP PROCEDURE IF EXISTS `rfm_instance_calculation`$$

CREATE DEFINER=`root`@`%` PROCEDURE `rfm_instance_calculation`(
	IN `fromtype` varchar(20),
	IN `fromvalue` int(2),
	IN `dftype` varchar(20),
	IN `dfval` int(2),
	IN `instance` varchar(20)
)
BEGIN
DECLARE dtThru	DATETIME;
DECLARE dtStart	DATETIME DEFAULT NOW();
DECLARE startDate DATETIME;
declare initdate datetime;
declare cntval int(10);
DECLARE sProcessId VARCHAR(255) DEFAULT 'rfm_calculation';
 
 
 /*Start date of given ranges*/
 
 
SET @stmt1:= (CONCAT('select DATE_ADD(DATE_sub(curdate(), INTERVAL ',fromvalue,'-1 ',fromtype,' ), interval -day(DATE_SUB(NOW(), INTERVAL ',fromvalue,'-1 ',fromtype,' )) +1 day) INTO @startDate'));
PREPARE stmt FROM @stmt1;
EXECUTE stmt;
-- SELECT @startDate ;
deallocate prepare stmt;	
 
-- SET @instance := (concat('Past ',fromvalue,' ', fromtype,' data from ',dfval,' ', dftype)); 
SET @instance := instance; 
 start_loop: loop
		
	
	
	/*Start date of data requirement*/
	#SELECT DATE_SUB(@startDate, INTERVAL dfval dftype) INTO initdate;
	
	set @intDate := (CONCAT('select date_sub(date(\'',@startDate,'\'), interval ',dfval,' ',dftype, ') into @initdate'));
	prepare stmt2 from @intDate;
	execute stmt2;
--	select @initdate;
	deallocate prepare stmt2;
-- set Id=
TRUNCATE rfm_fre;
INSERT INTO rfm_fre
	(`PARTY_ID`
	)
SELECT a.party_id FROM hsu_0004.party a, hsu_0004.party_role b WHERE a.party_id=b.party_id AND b.`ROLE_TYPE_ID`='CUSTOMER' 
AND a.status_id='PARTY_ENABLED';
set @upfre := (concat('UPDATE `rfm_fre` plc INNER JOIN
		(SELECT * FROM (
			SELECT COUNT(DISTINCT order_id) AS ORDER_CNT ,BILL_TO_PARTY_ID  FROM `bi_exp_transaction_opt_live` WHERE date(order_date)  BETWEEN 
			\'',@initdate,'\' AND last_day(\'',@startDate,'\') AND  TRANSACTION_TYPE IN(\'01\',\'04\') GROUP BY BILL_TO_PARTY_ID)  AS temp 
		)temp1
		SET plc.`Frequancy`=temp1.ORDER_CNT
		WHERE plc.party_id=temp1.BILL_TO_PARTY_ID'));
-- select @upfre;
prepare frestmt from @upfre;
execute frestmt;
deallocate prepare frestmt;
DELETE FROM rfm_fre WHERE `Frequancy` IS NULL;
UPDATE `rfm_fre` SET `PARTY_CLASSIFICATION_GROUP_ID`='FREQUENCY_SEQ_1' WHERE `Frequancy`=1;
UPDATE `rfm_fre` SET `PARTY_CLASSIFICATION_GROUP_ID`='FREQUENCY_SEQ_2' WHERE `Frequancy`=2;
UPDATE `rfm_fre` SET `PARTY_CLASSIFICATION_GROUP_ID`='FREQUENCY_SEQ_3' WHERE `Frequancy`>=3;
/*Recency update*/
TRUNCATE rfm_recency;
 INSERT INTO `rfm_recency` 
	(`PARTY_ID`
	)
SELECT a.party_id FROM hsu_0004.party a, hsu_0004.party_role b WHERE a.party_id=b.party_id AND b.`ROLE_TYPE_ID`='CUSTOMER' 
AND a.status_id='PARTY_ENABLED';
set @uprece := (concat('UPDATE `rfm_recency` plc INNER JOIN
			(SELECT * FROM (
			SELECT MAX(`ORDER_DATE`) AS ORDER_DATE ,BILL_TO_PARTY_ID  FROM `bi_exp_transaction_opt_live` WHERE order_date  BETWEEN 
			\'',@initdate,'\' AND LAST_DAY(\'',@startDate,'\') GROUP BY BILL_TO_PARTY_ID) AS temp
			)temp1
			SET plc.`ORDER_DATE`=temp1.ORDER_DATE
			WHERE plc.party_id=temp1.BILL_TO_PARTY_ID'));
select @uprece;
prepare restmt from @uprece;
execute restmt;
deallocate prepare restmt;
DELETE FROM `rfm_recency` WHERE `ORDER_DATE` IS NULL;
UPDATE `rfm_recency` SET `RECENCY_DAYS`=DATEDIFF(NOW(),`ORDER_DATE`);
UPDATE `rfm_recency` SET `PARTY_CLASSIFICATION_GROUP_ID`='RECENCY_SEQ_1' WHERE `RECENCY_DAYS`>=141; 
UPDATE `rfm_recency` SET `PARTY_CLASSIFICATION_GROUP_ID`='RECENCY_SEQ_2' WHERE `RECENCY_DAYS` BETWEEN 69 AND 140; 
UPDATE `rfm_recency` SET `PARTY_CLASSIFICATION_GROUP_ID`='RECENCY_SEQ_3' WHERE `RECENCY_DAYS` BETWEEN 0 AND 68;  
/*Monetory update*/
TRUNCATE rfm_spend;
INSERT INTO rfm_spend
	(`PARTY_ID`
	)
SELECT a.party_id FROM hsu_0004.party a, hsu_0004.party_role b WHERE a.party_id=b.party_id AND b.`ROLE_TYPE_ID`='CUSTOMER' 
AND a.status_id='PARTY_ENABLED';
set @upmo := (concat('UPDATE `rfm_spend` plc INNER JOIN
		(SELECT * FROM (
		SELECT SUM(QUANTITY_SOLD * ITEM_NET_SALES) AS ORDER_DATE , BILL_TO_PARTY_ID  FROM `bi_exp_transaction_opt_live` WHERE order_date  BETWEEN 
		\'',@initdate,'\' AND LAST_DAY(\'',@startDate,'\') AND  TRANSACTION_TYPE IN(\'01\',\'04\') GROUP BY BILL_TO_PARTY_ID)  AS temp
		)temp1
		SET plc.`TOTAL_SALES_AMOUNT`=temp1.ORDER_DATE
		WHERE plc.party_id=temp1.BILL_TO_PARTY_ID'));
select @upmo;
prepare mostmt from @upmo;
execute mostmt;
deallocate prepare mostmt;
		
DELETE FROM `rfm_spend` WHERE `TOTAL_SALES_AMOUNT`<=0;
DELETE FROM `rfm_spend` WHERE `TOTAL_SALES_AMOUNT` IS NULL;
UPDATE `rfm_spend` SET `PARTY_CLASSIFICATION_GROUP_ID`='SPEND_RANGE_1' WHERE `TOTAL_SALES_AMOUNT` BETWEEN 0.01 AND 293.44; 
UPDATE `rfm_spend` SET `PARTY_CLASSIFICATION_GROUP_ID`='SPEND_RANGE_2' WHERE `TOTAL_SALES_AMOUNT` BETWEEN 293.45 AND 788.12; 
UPDATE `rfm_spend` SET `PARTY_CLASSIFICATION_GROUP_ID`='SPEND_RANGE_3' WHERE `TOTAL_SALES_AMOUNT` >=788.13;
UPDATE bi_customer_master_opt_live a,rfm_recency b
SET a.CURRENT_RECENCY = b.PARTY_CLASSIFICATION_GROUP_ID
WHERE a.PARTY_ID=b.PARTY_ID;
UPDATE bi_customer_master_opt_live a,rfm_spend b
SET a.CURRENT_MONETORY = b.PARTY_CLASSIFICATION_GROUP_ID
WHERE a.PARTY_ID=b.PARTY_ID;
UPDATE bi_customer_master_opt_live a,rfm_fre b
SET a.`CURRENT_FREQUENCY` = b.PARTY_CLASSIFICATION_GROUP_ID
WHERE a.PARTY_ID=b.PARTY_ID;
INSERT INTO `rfm_summary_copy` (`INSTANCE_ID`, `CUST_ID`, `RFM_SEQ_ID`,
`RFM_YEAR`, `RFM_QUARTER`, `RFM_MONTH`, `RFM_WEEK`, `RFM_DATE`, `RFM_SCORE`, `RECENCY`, `FREQUENCY`, `MONETORY`, `SEGMENT_CODE1`, `SEGMENT_CODE2`, `SEGMENT_CODE3`, `SEGMENT_CODE4`, `SEGMENT_CODE5`, `SEGMENT_CODE6`, `SEGMENT_CODE7`, `SEGMENT_CODE8`, `SEGMENT_CODE9`, `SEGMENT_CODE10`, `SEGMENT_CODE11`, `SEGMENT_CODE12`, `SEGMENT_CODE13`, `SEGMENT_CODE14`, `SEGMENT_CODE15`, `SEGMENT_CODE16`, `SEGMENT_CODE17`)
SELECT @instance, PARTY_ID,GET_SEQ_ID('RFMSEQID'),YEAR(LAST_DAY(@startDate)) AS yr,
		 QUARTER(LAST_DAY(@startDate)) AS RFM_QUARTER, MONTH(LAST_DAY(@startDate)) AS mnth, WEEK((@startDate)) AS wk, LAST_DAY(@startDate) AS dt,
		 CONCAT(RIGHT(`CURRENT_RECENCY`,1), RIGHT(`CURRENT_FREQUENCY`,1), RIGHT(`CURRENT_MONETORY`,1)) AS SCR,`CURRENT_RECENCY` AS R, 
		`CURRENT_FREQUENCY` AS F, `CURRENT_MONETORY` AS M, AGE_GROUP, 'U', CUSTOMER_LIFE_CYCLE, STATE_PROVINCE_GEO_ID,POSTAL_CODE,
		 MAILER_SEGMENT_CODE_ID1, MAILER_SEGMENT_CODE_ID2,MAILER_SEGMENT_CODE_ID3, MAILER_SEGMENT_CODE_ID4, MAILER_SEGMENT_CODE_ID5,MAILER_SEGMENT_CODE_ID6,
		 MAILER_SEGMENT_CODE_ID7,MAILER_SEGMENT_CODE_ID8, MAILER_SEGMENT_CODE_ID9, MAILER_SEGMENT_CODE_ID10,
       MAILER_SEGMENT_CODE_ID11, MAILER_SEGMENT_CODE_ID12	
   FROM `bi_customer_master_opt_live`
	WHERE CURRENT_RECENCY IS NOT NULL AND CURRENT_FREQUENCY IS NOT NULL AND CURRENT_MONETORY IS NOT NULL
	GROUP BY PARTY_ID;
	
TRUNCATE sales_avg_temp;
INSERT INTO `sales_avg_temp` (`Party_id`, `TRANS_VAL`, `Trans_count`,avg_sales, `order_date`) 
SELECT BILL_TO_PARTY_ID AS PARTY_ID,(total_sales_amount) AS TRANS_VAL,COUNT(DISTINCT order_id) AS Trans_count,((total_sales_amount)/COUNT(DISTINCT order_id)) AS avg_sales,order_date  FROM bi_exp_transaction_opt_live
WHERE  date(order_date) BETWEEN  DATE(@initdate) AND  DATE(LAST_DAY(@startDate)) AND  TRANSACTION_TYPE IN('01','04') GROUP BY BILL_TO_PARTY_ID;
UPDATE rfm_summary_copy a , sales_avg_temp b
SET a.TRANS_VAL = b.TRANS_VAL,
	 a.TRANS_COUNT = b.Trans_count,
	 a.AVG_SLS = b.avg_sales
 WHERE  a.cust_id = b.party_id and date(a.RFM_DATE)= date(LAST_DAY(@startDate))
 AND @INSTANCE = a.INSTANCE_ID ;
SET @stDate := (CONCAT('select date_add(\'',@startDate,'\', interval 1 ', fromtype,') into @startDate'));
PREPARE stmt3 FROM @stDate;
EXECUTE stmt3;
deallocate prepare stmt3;
IF date(@startDate) > CURDATE() THEN 
	LEAVE start_loop;  
END IF;
iterate start_loop;
end loop;
set cntval = (select count(distinct cust_id) from `rfm_summary_copy` where `INSTANCE_ID` = @instance);
Update `rfm_instance` set `STATUS` = 'Ready', `NUMBER_OF_CUSTOMER` = cntval  where `RFM_INSTANCE_ID` = @instance;
-- call clv_instance_calculation(fromtype, fromvalue);
END$$

DELIMITER ;